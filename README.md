# markdownAPI

Python Flask app that manages a library of markdown documents and gives API access.
Designed to deliver content to a web app such as a blog or training manual.
Some pre-processing of the documents is done in a flask-like way: insert block of text