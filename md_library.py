import glob
from pathlib import Path, PurePath


def create_folder_index(topFolder, folderIgnoreList):
    '''
    Returns a dict with all .md files in all subfolders
    Ignores folders named in folderIgnoreList
    '''
    newDict = tempDict = dict()
    # tunnel recursively into library
    tempDict["files"] = list()
    for item in topFolder.iterdir():
        if item.is_dir():
            if item.name[0] == '.':
                # print(f"skipping {item.name}")
                pass
            elif item.name in folderIgnoreList:
                # print(f"skipping {item.name}")
                pass
            else:
                # print(f"adding {item.name}")
                newDict[item.name] = create_folder_index(item, folderIgnoreList)

        else:
            # add the files key and the filenames as a list
            # print(item.name)
            if item.name[-3:] == ".md":
                newDict['files'].append(item.name)
            # add the subFolders
    
    return newDict


def create_document_index(topFolder, folderIgnoreList):
    '''
    Recursively scan the folder for .md files
    the folders listed will be ignored in the top level
    return a list of tuple (filename, fullpath)
    '''
    documentDict = dict()
    docID = 0
    for folder in topFolder.iterdir():
        if folder in folderIgnoreList:
            # print(f"skipping {folder}")
            pass
        else:
            pathList = folder.rglob("*.md")
            for docPath in pathList:
                docName = docPath.parts[-1].split(".")[0]
                # print(f"Document Name: {docName}")
                docID += 1
                docKey = str(docID)
                documentDict[docKey] = dict()
                documentDict[docKey]["title"] = docName
                documentDict[docKey]["path"] = docPath
    return documentDict


def print_library_tree(mdDict, textString, indent, lineBreak):
    '''
    recursively prints the structure of a dict of dicts
    TODO: create HTML version with document <id> href
        and make this possible to request docs from the API
    '''

    keyList = mdDict.keys()
    if len(keyList) > 0:
        indent += indent
        for key in keyList:
            if isinstance(mdDict[key], list):
                fileList = mdDict["files"]
                if len(fileList) > 0:
                    for fileName in fileList:
                        line = (f"{indent}- {fileName}")
                        print(line)
                        textString += line + lineBreak
                
            elif isinstance(mdDict[key], dict):
                line = (f"{indent}+ {key}")
                print(line)
                textString += line + lineBreak
                print_library_tree(mdDict[key], textString, indent, lineBreak)
            
            else:
                print(f"skipping {key} of type: {type(mdDict[key])}")
    
    # print(textString)
    return(textString)


if __name__ == "__main__":
    print("This file should not be run directly")  
    
else:
    print(f"Importing {__name__}")
