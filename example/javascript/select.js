const api_url = 'http://localhost:5000/validOS'
/* access the API */
// need to enable CORS on localhost (I used firefox extension "cors everywhere ")


const GetValidOS = async () => {
    const response = await fetch(api_url);
    const data = await response.json(); //extract JSON from the http response
    //     do something with json data
    console.log(data);
}

var validOS = GetValidOS


var selector, i, j, selectorLength, selectorElementLength, selectorElement, newDiv, b, c;
/* Look for any elements with the class "custom-select": */
selector = document.getElementsByClassName("custom-select");

selectorLength = selector.length;
for (i = 0; i < selectorLength; i++) {
    selectorElement = selector[i].getElementsByTagName("select")[0];
    selectorElementLength = selectorElement.length;
    /* For each element, create a new DIV that will act as the selected item: */
    newDiv = document.createElement("DIV");
    newDiv.setAttribute("class", "select-selected");
    newDiv.innerHTML = selectorElement.options[selectorElement.selectedIndex].innerHTML;
    selector[i].appendChild(newDiv);
    /* For each element, create a new DIV that will contain the option list: */
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selectorElementLength; j++) {
        /* For each option in the original select element,
         create a new DIV that will act as an option item*: */
        c = document.createElement("DIV");
        c.innerHTML = selectorElement.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /* When an item is clicked, update the original select box,
             a nd the selected item: */
             var y, i, k, s, h, sl, yl;
             s = this.parentNode.parentNode.getElementsByTagName("select")[0];
             sl = s.length;
             h = this.parentNode.previousSibling;
             for (i = 0; i < sl; i++) {
                 if (s.options[i].innerHTML == this.innerHTML) {
                     s.selectedIndex = i;
                     h.innerHTML = this.innerHTML;
                     y = this.parentNode.getElementsByClassName("same-as-selected");
                     yl = y.length;
                     for (k = 0; k < yl; k++) {
                         y[k].removeAttribute("class");
                     }
                     this.setAttribute("class", "same-as-selected");
                     break;
                 }
             }
             h.click();
        });
        b.appendChild(c);
    }
    selector[i].appendChild(b);
    newDiv.addEventListener("click", function(e) {
        /* When the select box is clicked, close any other select boxes,
         a nd open/close the current select box: */
         e.stopPropagation();
         closeAllSelect(this);
         this.nextSibling.classList.toggle("select-hide");
         this.classList.toggle("select-arrow-active");
    });
}

function closeAllSelect(elmnt) {
    /* A function that will close all select boxes in the document,
     e xcept the current select box: */
     var x, y, i, xl, yl, arrNo = [];
     x = document.getElementsByClassName("select-items");
     y = document.getElementsByClassName("select-selected");
     xl = x.length;
     yl = y.length;
     for (i = 0; i < yl; i++) {
         if (elmnt == y[i]) {
             arrNo.push(i)
         } else {
             y[i].classList.remove("select-arrow-active");
         }
     }
     for (i = 0; i < xl; i++) {
         if (arrNo.indexOf(i)) {
             x[i].classList.add("select-hide");
         }
     }
}

/* If the user clicks anywhere outside the select box,
 t hen close all select boxes: */
document.addEventListener("click", closeAllSelect); 



