#!/usr/bin/python3
import json
from flask import Flask, request, jsonify
from pathlib import Path, PurePath
# markdownapi modules
import md_library

# print(f"This File: {Path(__file__)}")
# define the baseDir and load config
baseDir = Path(__file__).resolve().parent
print(f"Base Folder: {baseDir}")
parentDir = Path(__file__).resolve().parents[1]
# print(f"Parent Folder: {parentDir}")
configFileName = "markdownapi.conf"
app = Flask(__name__)

# print(f"Library Contents:")
# printLibraryTree(mdDict, "")

validOS = {"windows": 
        {"version": ["7", "10"]}, 
            "linux": {
            "debian": {"version": ["9", "10"]}, 
            "ubuntu": {"version": ["16.04", "18.04", "20.04"]}
            },
            "macos": 
            {"version": ["10", "10.1"]}
        }


def main():
    # check config file
    options = load_config(baseDir, configFileName)
    # set options from config
    # TODO: 
    libraryFolder = Path(options["LIBRARY"][0])
    # print(f"Library Folder: {libraryFolder}")

    folderIgnoreList = options["IGNORE"]

    mdDict = md_library.create_folder_index(libraryFolder, folderIgnoreList)
    if Path(libraryFolder).is_dir():
        # print(f"Found Library at: {libraryFolder}")
        pass
    else:
        print(f"Creating Library at: {libraryFolder}")

    # print(f"Scanning subfolders")


def load_config(baseDir, configFileName):
    options = dict()
    configFile = Path.joinpath(baseDir, configFileName)
    with open(configFile, 'r') as f:
        for line in f.readlines():
            if line[0] == "#":
                # print(f"Skipping commented line")
                pass
            elif ":" in line:
                configItem, configValue = line.split(":")
                configItem = configItem.strip()
                options[configItem] = list()
                options[configItem].append(configValue.strip())
    
    # print(options)
    
    return options


def setup_library():
    options = load_config(baseDir, configFileName)
    # set options from config
    libraryFolder = Path(options["LIBRARY"][0])
    print(f"GET /index - Library Folder: {libraryFolder}")
    folderIgnoreList = options["IGNORE"]
    print(f"GET /index - Ignore Folders: {folderIgnoreList}")
    return libraryFolder, folderIgnoreList


@app.route('/')
def webroot():
    pass


@app.route('/validOS')
def valid_os():
    return jsonify(validOS)


@app.route('/tree')
def library_tree():
    libraryFolder, folderIgnoreList = setup_library()
    mdDict = md_library.create_folder_index(libraryFolder, folderIgnoreList)
    indent = "  "
    lineBreak = "<br>"  # for HTML, TODO: other options?
    # TODO: the following function should return HTML
    textString = "<p>"
    textString += md_library.print_library_tree(mdDict, textString, indent, lineBreak)
    textString += "</p>"
    return textString

@app.route('/index', methods=['GET'])
def get_index():
    '''
    return the entire library index
    '''
    libraryFolder, folderIgnoreList = setup_library()
    mdDict = md_library.create_folder_index(libraryFolder, folderIgnoreList)
    return jsonify(mdDict)


@app.route('/docID/<docID>', methods=['GET'])
def get_document(docID):
    '''
    return the document with docID = <id>
    '''
    params = request.args
    print(f"params: {jsonify(params)}")
    
    # store the GET arguments and validate
    # TODO: deal with na options
    try:
        osName = params.get('os').lower()
    except Exception as e:
        print("Operating System not selected")
        osName = ""
    try:
        distroName = params.get('distro').lower()
    except Exception as e:
        print("Distro not selected")
        distroName = ""
    try:
        osVersion = params.get('osVersion').lower()
    except Exception as e:
        print("OS Version not selected")
        osVersion = ""
    
    # check and modify if necessary
    osName, distroName, osVersion = checkValidParams(osName, distroName, osVersion)
    
    
    # load the document
    libraryFolder, folderIgnoreList = setup_library()
    documentDict = md_library.create_document_index(libraryFolder, folderIgnoreList)
    # print(documentDict)
    
    with open(documentDict[docID]["path"], 'r') as f:
        documentText = f.read()
        
    # process the insertable blocks in the document
    newText = replaceTextBlock(documentText, osName, distroName, osVersion)
    return newText


def checkValidParams(osName, distroName, osVersion):
    if osName in validOS.keys():
        print(f"found osName: {osName}")
        if osName == 'linux':
            # print(f"Valid Distros: {validOS[osName].keys()}")
            if distroName in validOS[osName].keys(): 
                print(f"Distro Name: {distroName}")
                if osVersion in validOS[osName][distroName]['version']: 
                    print(f"OS Version: {osVersion}")
                else:
                    osVersion = ""
                    print(f"Please select a different Distro Version")
                
            else:
                distroName = ""
                print(f"Generic Linux Distro")
        
        elif osName == 'windows':
            if osVersion in validOS[osName]['version']: 
                print(f"OS Version: {osVersion}")
            else:
                osVersion = ""
                print(f"Please select a different Operating System Version")
        elif osName == 'macos':
            if osVersion in validOS[osName]['version']: 
                print(f"OS Version: {osVersion}")
            else:
                osVersion = ""
                print(f"Please select a different Operating System Version")

    else:
        print(f"Please select a different Operating System")
    
    return osName, distroName, osVersion


def replaceTextBlock(documentText, osName, distroName, osVersion):
    # newText = documentText
    print(f"os: {osName}, dist: {distroName}, ver: {osVersion}")
    try:
        mainText = documentText.split("{{", 1)
        print(mainText[0])
        try:
            tempText = mainText[1].split("}}", 1)
        except Exception as e:
            raise e
        # got an open and close block "{{   }}"
        print(f"Found Text-replace block: {tempText[0]}")
        # TODO: run the replace function
        newVar = "TEXT REPLACED"
        # iterate on the remaining text
        newText = mainText[0] + newVar + replaceTextBlock(tempText[1], osName, distroName, osVersion)
    
    except Exception:
        newText = documentText
        print("finished replace blocks")
    
    return newText




    
if __name__ == "__main__":
    main()
    app.run(debug=True)    
    
else:
    print(f"Importing {__name__}")
